# бібліотеки для парсінгу сторінки
require 'open-uri'
require 'nokogiri'

url = 'http://www.imdb.com/search/title?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc_2'
html = open(url)

doc = Nokogiri::HTML(html)

# кожен елемент списку має клас '.lister-item-content' в html-структурі, отже витягуємо їх по черзі
lister_items = []
doc.css('.lister-item-content').each do |lister_item|
  runtime = lister_item.css('.runtime').text # довжина фільму, знаходимо за класом
  genre = lister_item.css('.genre').text.strip.split(', ') # жанр, так само, але їх може бути декілька
  # рейтинги
  imdb_rating = lister_item.css('.ratings-imdb-rating strong').text.to_f
  metascore_rating = lister_item.css('.metascore').text.to_i
  # опис фільму, видаляємо непотрібний текст
  description = lister_item.css('p')[1].text.gsub('                See full summary »', '').strip
  director = lister_item.css('p')[2].css('a')[0].text # режисерр, витягуємо перший елемент "а" з другого "р"
  stars = [] # актори, послідовно витягуємо, йдуть за елементом з класом '.ghost'
  stars[0] = lister_item.css('p')[2].css('.ghost')[0].next.next.text
  stars[1] = lister_item.css('p')[2].css('.ghost')[0].next.next.next.next.text
  stars[2] = lister_item.css('p')[2].css('.ghost')[0].next.next.next.next.next.next.text
  stars[3] = lister_item.css('p')[2].css('.ghost')[0].next.next.next.next.next.next.next.next.text
  # кількість голосів
  votes = lister_item.css('.sort-num_votes-visible').css('span')[1]['data-value'].to_f
  # бюджет
  gross = lister_item.css('.sort-num_votes-visible').css('span').last.text.split('$').last.split('M').last.to_f
  # не у всіх фільмів вказаний бюджет, тому потрібна перевірка кількості елементів у структурі '.sort-num_votes-visible'
  if lister_item.css('.sort-num_votes-visible').children.count < 11
    gross = 0
  end

  lister_items.push(
                  runtime: runtime,
                  genre: genre,
                  imdb_rating: imdb_rating,
                  metascore_rating: metascore_rating,
                  description: description,
                  director: director,
                  stars: stars,
                  votes: votes,
                  gross: gross
  )
end

# струтура з відомостями, витягнутими з веб-сторінки
puts lister_items

# визначення, який жанр має мати фільм, щоб перемогти
genre_filtration = lister_items.each_with_object(Hash.new {|h, k| h[k] = []}) {|h, result| h[:genre].each {|n| result[n] << h}}
genre_rating = genre_filtration.map {|key, value| [key, value.size]}.sort_by {|_, v| -v}.to_h

# який мінімальний рейтинг на IMDb проходив
lowest_imdb_rating = lister_items.min{|a,b| a[:imdb_rating] <=> b[:imdb_rating]}

# які зірки найчастіше знімалися у фільмах, що перемагали
stars_filtration = lister_items.each_with_object(Hash.new {|h, k| h[k] = []}) {|h, result| h[:stars].each {|n| result[n] << h}}
stars_rating = stars_filtration.map {|key, value| [key, value.size]}.sort_by {|_, v| -v}.to_h

# усі інші якості фільмів відрізняються між собою, тому їх оцінка здалася мені недоцільною
puts "\nAs a result, to be a winner, film must be #{genre_rating.keys.first} - #{(genre_rating.values.first * 100)/90.to_f}% guarantee,
       minimum appropriate IMDb Rating is #{lowest_imdb_rating[:imdb_rating]},
       and such stars " + stars_rating.select{|_, v| v > 2}.keys.join(', ') + ' also usually bring success.'